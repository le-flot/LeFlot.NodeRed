LeFlot-NodeRed
==============

### About

Ceci est le répertoire du patch Node-Red.

## Version

node-red 2.0.5 (node.js 14.17.1)

## Réinstallation

Aller à l'interface Node-Red à l'adresse localhost:1880.

Dans settings/Git Config, ajouter une clé SSH.

Copier cette clé dans le répertoire GitLab.

Dans projects, new projects, cloner le répertoire en entrant les informations nécessaires.
